<div class="site-info">
	<div class="logo">
		<a href="<?php echo site_url(); ?>">
			<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="project-info">
		<h2><?php echo get_field('project_name', 'options'); ?></h2>
		<h1><?php echo get_field('project_type', 'options'); ?></h1>

		<h5>
			<?php echo get_field('preparation_info', 'options'); ?><br/>
			<?php echo get_field('preparation_date', 'options'); ?>
		</h5>
	</div>
</div>