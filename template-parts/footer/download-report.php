<?php $file = get_field('full_report', 'options'); if( $file ): ?>
    <div class="download-report">

        <a href="<?php echo $file['url']; ?>" rel="external">
            <span class="icon">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-pdf-large.svg" alt="PDF" />
            </span>

            <span class="meta">
                <span class="cta">Download the Full Report</span>
                <span class="details"><?php echo get_field('full_report_details', 'options'); ?></span>
            </span>
        </a>

    </div>
<?php endif; ?>